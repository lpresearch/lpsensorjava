
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import javax.swing.GroupLayout.*;

import com.lpresearch.LpmsB2;
import com.lpresearch.LpmsData;

public class LpmsExample extends JFrame {
    
    private JTextField entry;
    private JLabel jLabel1;
    private JScrollPane jScrollPane1;
    private JLabel status;
    private JTextArea textArea;
    
    final static Color  ERROR_COLOR = Color.PINK;
    final static String ENTER_ACTION = "execute-command";
    
    final Color entryBg;

    // Sensor states
    static LpmsB2 lpSensor = new LpmsB2();
    final static int MAIN_MENU = 0;
    final static int CONNECTION_MENU = 1;
    final static int SET_IMU_ID_MENU = 2;
    final static int SET_GYRO_RANGE_MENU = 3;
    final static int SET_ACC_RANGE_MENU = 4;
    final static int SET_MAG_RANGE_MENU = 5;
    final static int SET_STREAM_FREQ_MENU = 6;
    final static int SET_FILTER_MODE_MENU = 7;
    final static int PRINT_DATA = 8;
    private int currentMenuState = MAIN_MENU;
    

    final static String mainMenu = "*********************\n"
                        + "[c] Connect\n"
                        + "[d] Disconnect\n"
                        + "[0] Get config register\n"
                        + "[1] Set command mode\n"
                        + "[2] Set streaming mode\n"
                        + "[3] Set IMU Id\n"
                        + "[4] Set gyro range\n"
                        + "[5] Set acc range\n"
                        + "[6] Set mag range\n"
                        + "[7] Set stream frequency\n"
                        + "[8] Set filter mode\n"
                        + "[9] Get battery level\n"
                        + "[10] Get charging status\n"
                        + "[p] Print_data\n"
                        + "*********************\n";

    final static String connectMenu = "Enter COM Port: (eg. COM1, COM2...)\n" 
                                    + "[q] return to main menu\n";

    final static String setImuIdMenu = "Enter IMU ID: \n"
                                    + "[q] return to main menu\n";

    final static String setAccRangeMenu = "Enter acc range: \n"
                                    + "[0] 2G\n"
                                    + "[1] 4G\n"
                                    + "[2] 8G\n"
                                    + "[3] 16G\n"
                                    + "[q] return to main menu\n";
    
    final static String setGyroRangeMenu = "Enter gyro range: \n"
                                    + "[0] 125dps\n"
                                    + "[1] 245dps\n"
                                    + "[2] 500dps\n"
                                    + "[3] 1000dsp\n"
                                    + "[4] 2000dsp\n"
                                    + "[q] return to main menu\n";

    final static String setMagRangeMenu = "Enter mag range: \n"
                                    + "[0] 4Gauss\n"
                                    + "[1] 8Gauss\n"
                                    + "[2] 12Gauss\n"
                                    + "[3] 16Gauss\n"
                                    + "[q] return to main menu\n";

    final static String setStreamFreqMenu = "Enter stream frequency: \n"
                                    + "[0] 5Hz\n"
                                    + "[1] 10Hz\n"
                                    + "[2] 25Hz\n"
                                    + "[3] 50Hz\n"
                                    + "[4] 100Hz\n"
                                    + "[5] 200Hz\n"
                                    + "[6] 400Hz\n"
                                    + "[q] return to main menu\n";

    final static String setFilterModeMenu = "Enter filter mode: \n"
                                    + "[0] Gyro only\n"
                                    + "[1] Gyro + Acc (Kalman)\n"
                                    + "[2] Gyro + Acc + Mag (Kalman)\n"
                                    + "[3] Gyro + Acc (DCM)\n"
                                    + "[4] Gyro + Acc + Mag (DCM)\n"
                                    + "[q] return to main menu\n";

    public LpmsExample() {
        initComponents();
        printMainMenu();
        entryBg = entry.getBackground();
        InputMap im = entry.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap am = entry.getActionMap();
        im.put(KeyStroke.getKeyStroke("ENTER"), ENTER_ACTION);
        am.put(ENTER_ACTION, new ExecuteAction());
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     */

    private void initComponents() {
        entry = new JTextField();
        textArea = new JTextArea();
        status = new JLabel();
        jLabel1 = new JLabel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("LpmsExample");

        textArea.setColumns(20);
        textArea.setLineWrap(true);
        textArea.setRows(15);
        textArea.setWrapStyleWord(true);
        textArea.setEditable(false);
        jScrollPane1 = new JScrollPane(textArea);

        jLabel1.setText("Command:");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        
        //Create a parallel group for the horizontal axis
        ParallelGroup hGroup = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
        
        //Create a sequential and a parallel groups
        SequentialGroup h1 = layout.createSequentialGroup();
        ParallelGroup h2 = layout.createParallelGroup(GroupLayout.Alignment.TRAILING);
        
        //Add a container gap to the sequential group h1
        h1.addContainerGap();
        
        //Add a scroll pane and a label to the parallel group h2
        h2.addComponent(jScrollPane1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE);
        h2.addComponent(status, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE);
        
        //Create a sequential group h3
        SequentialGroup h3 = layout.createSequentialGroup();
        h3.addComponent(jLabel1);
        h3.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED);
        h3.addComponent(entry, GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE);
         
        //Add the group h3 to the group h2
        h2.addGroup(h3);
        //Add the group h2 to the group h1
        h1.addGroup(h2);

        h1.addContainerGap();
        
        //Add the group h1 to the hGroup
        hGroup.addGroup(GroupLayout.Alignment.TRAILING, h1);
        //Create the horizontal group
        layout.setHorizontalGroup(hGroup);
        
            
        //Create a parallel group for the vertical axis
        ParallelGroup vGroup = layout.createParallelGroup(GroupLayout.Alignment.LEADING);
        //Create a sequential group v1
        SequentialGroup v1 = layout.createSequentialGroup();
        //Add a container gap to the sequential group v1
        v1.addContainerGap();
        //Create a parallel group v2
        ParallelGroup v2 = layout.createParallelGroup(GroupLayout.Alignment.BASELINE);
        v2.addComponent(jLabel1);
        v2.addComponent(entry, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE);
        //Add the group v2 tp the group v1
        v1.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE);
        v1.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED);
        v1.addGroup(v2);
        v1.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED);
        v1.addComponent(status);
        v1.addContainerGap();
        
        //Add the group v1 to the group vGroup
        vGroup.addGroup(v1);
        //Create the vertical group
        layout.setVerticalGroup(vGroup);
        pack();
        entry.requestFocus();
    }


    void message(String msg) {
        status.setText(msg);
    }

    void errorMessage(String msg) {
        entry.setBackground(ERROR_COLOR);
        status.setText(msg);
    }
    

    class ExecuteAction extends AbstractAction {
        public void actionPerformed(ActionEvent ev) {
            parseInput(entry.getText());
            entry.setText("");
            entry.setBackground(entryBg);
        }
    }

    // Menu
    void printMainMenu()
    {
        currentMenuState = MAIN_MENU;
        print(mainMenu, false);
    }

    void printConnectMenu() {
        currentMenuState = CONNECTION_MENU;
        print(connectMenu, false);
    }

    void printSetImuIdMenu() {
        currentMenuState = SET_IMU_ID_MENU;
        print(setImuIdMenu, false);
    }

    void printSetAccRangeMenu() {
        currentMenuState = SET_ACC_RANGE_MENU;
        print(setAccRangeMenu, false);
    }

    void printSetGyroRangeMenu() {
        currentMenuState = SET_GYRO_RANGE_MENU;
        print(setGyroRangeMenu, false);
    }

    void printSetMagRangeMenu() {
        currentMenuState = SET_MAG_RANGE_MENU;
        print(setMagRangeMenu, false);
    }

    void printSetStreamFreqMenu() {
        currentMenuState = SET_STREAM_FREQ_MENU;
        print(setStreamFreqMenu, false);
    }

    void printSetFilterModeMenu() {
        currentMenuState = SET_FILTER_MODE_MENU;
        print(setFilterModeMenu, false);
    }

    void printConfigRegister() {
        String config  = lpSensor.getConfigString();
        print("Sensor Config Register:\n", true);
        print(config + "\n", true);
    }


    boolean isStopThread = false;
    Thread printThread;
    void startPrintData() {
        currentMenuState = PRINT_DATA;
        isStopThread = false;

        printThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (lpSensor.getConnectionStatus()==LpmsB2.SENSOR_STATUS_CONNECTED && !isStopThread){
                        int n = lpSensor.hasNewData();
                        if (n > 0) {
                            LpmsData d = new LpmsData();

                            // Clear internal sensor queue to obtain latest IMU data
                            // not neccessary is data reading loop is fast enough
                            while (n-- > 0) {
                                d = lpSensor.getLpmsData();
                            }
                            if (d != null) {
                                String data = String.format("Timestamp: %f\n", d.timestamp); 
                                data += String.format("Acc(G) X: %+.3f Y: %+.3f Z: %+.3f\n", d.acc[0], d.acc[1], d.acc[2]);
                                data += String.format("Gyr(deg/s) X: %+.3f Y: %+.3f Z: %+.3f\n", d.gyr[0], d.gyr[1], d.gyr[2]);
                                data += String.format("Mag(uT) X: %+.3f Y: %+.3f Z: %+.3f\n", d.mag[0], d.mag[1], d.mag[2]);
                                data += String.format("AngVel(deg/s) X: %+.3f Y: %+.3f Z: %+.3f\n", d.angVel[0], d.angVel[1], d.angVel[2]);
                                data += String.format("Quat W: %+.3f X: %+.3f Y: %+.3f Z: %+.3f\n", d.quat[0], d.quat[1], d.quat[2], d.quat[3]);
                                data += String.format("Euler(deg) X: %+.3f Y: %+.3f Z: %+.3f\n", d.euler[0], d.euler[1], d.euler[2]);
                                data += String.format("LinAcc(G) X: %+.3f Y: %+.3f Z: %+.3f\n", d.linAcc[0], d.linAcc[1], d.linAcc[2]);
                                data += String.format("Pressure(kPa) %+.3f\n", d.pressure);
                                data += String.format("Altitude(m) %+.3f\n", d.altitude);
                                data += String.format("Temperature(deg) %+.3f\n", d.temperature);
                                data += String.format("Battery %.3f%%  %.3fV\n", d.batteryLevel, d.batteryVoltage);
                                data += String.format("Charging status: %s\n", d.chargingStatus==0?"Not charging":"Charging");
                                print(data, false);
                            }
                        }
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        );
        printThread.start();
    }

    void stopPrintData() {

        isStopThread = true;
        try {
            printThread.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
    }

    void print(String msg, boolean append) {
        if (append) {
            textArea.append(msg);
        } else {
           textArea.setText(msg);
        }
    }

    void parseInput(String msg) {
        int range;
        switch (currentMenuState) {
            case MAIN_MENU:
                if (msg.equals("c")) {
                    printConnectMenu(); 
                } else if (msg.equals("d")) {
                    if (disconnectSensor()) {
                        message("Sensor disconnected");
                        printMainMenu();
                    } else {
                        errorMessage("Error disconnecting sensor");
                    }
                } else if (msg.equals("0")) {
                    printMainMenu();
                    printConfigRegister();
                } else if (msg.equals("1")) {
                    setCommandMode();
                } else if (msg.equals("2")) {
                    setStreamingMode();
                } else if (msg.equals("3")) {
                    printSetImuIdMenu();
                } else if (msg.equals("4")) {
                    printSetGyroRangeMenu();
                } else if (msg.equals("5")) {
                    printSetAccRangeMenu();
                } else if (msg.equals("6")) {
                    printSetMagRangeMenu();
                } else if (msg.equals("7")) {
                    printSetStreamFreqMenu();
                } else if (msg.equals("8")) {
                    printSetFilterModeMenu();
                } else if (msg.equals("9")) {
                    getBatteryLevel();
                } else if (msg.equals("10")) {
                    getChargingStatus();
                } else if (msg.equals("p")) {
                    startPrintData();
                }
                break;

            case CONNECTION_MENU:
                if (msg.equals("q")) {
                    printMainMenu();
                    break;
                }

                if (connectSensor(msg)) {
                    message("Sensor connected");
                    printMainMenu();
                    printConfigRegister();
                } else {
                    errorMessage("Error connecting to " + msg);
                }
                break;

            case SET_IMU_ID_MENU:
                if (msg.equals("q")) {
                    printMainMenu();
                    break;
                }
                if (lpSensor.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED) {
                    errorMessage("Sensor not connected");
                    break;
                }
                setImuId(Integer.parseInt(msg));
                printMainMenu();
                break;

            case SET_GYRO_RANGE_MENU:
                if (msg.equals("q")) {
                    printMainMenu();
                    break;
                }
                if (lpSensor.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED) {
                    errorMessage("Sensor not connected");
                    break;
                }
                if (msg.equals("0")) {
                    range = LpmsB2.LPMS_GYR_RANGE_125DPS;
                } else if (msg.equals("1")) {
                    range = LpmsB2.LPMS_GYR_RANGE_245DPS;
                } else if (msg.equals("2")) {
                    range = LpmsB2.LPMS_GYR_RANGE_500DPS;
                } else if (msg.equals("3")) {
                    range = LpmsB2.LPMS_GYR_RANGE_1000DPS;
                } else if (msg.equals("4")) {
                    range = LpmsB2.LPMS_GYR_RANGE_2000DPS;
                } else {
                    errorMessage("Invalid selection, try again");
                    break;
                }
                setGyroRange(range);
                printMainMenu();
                break;

            case SET_ACC_RANGE_MENU:
                if (msg.equals("q")) {
                    printMainMenu();
                    break;
                }
                if (lpSensor.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED) {
                    errorMessage("Sensor not connected");
                    break;
                }
                if (msg.equals("0")) {
                    range = LpmsB2.LPMS_ACC_RANGE_2G;
                } else if (msg.equals("1")) {
                    range = LpmsB2.LPMS_ACC_RANGE_4G;
                } else if (msg.equals("2")) {
                    range = LpmsB2.LPMS_ACC_RANGE_8G;
                } else if (msg.equals("3")) {
                    range = LpmsB2.LPMS_ACC_RANGE_16G;
                } else {
                    errorMessage("Invalid selection, try again");
                    break;
                }
                setAccRange(range);
                printMainMenu();
                break;

            case SET_MAG_RANGE_MENU:
                if (msg.equals("q")) {
                    printMainMenu();
                    break;
                }
                if (lpSensor.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED) {
                    errorMessage("Sensor not connected");
                    break;
                }
                if (msg.equals("0")) {
                    range = LpmsB2.LPMS_MAG_RANGE_4GAUSS;
                } else if (msg.equals("1")) {
                    range = LpmsB2.LPMS_MAG_RANGE_8GAUSS;
                } else if (msg.equals("2")) {
                    range = LpmsB2.LPMS_MAG_RANGE_12GAUSS;
                } else if (msg.equals("3")) {
                    range = LpmsB2.LPMS_MAG_RANGE_16GAUSS;
                } else {
                    errorMessage("Invalid selection, try again");
                    break;
                }
                setMagRange(range);
                printMainMenu();
                break;

            case SET_STREAM_FREQ_MENU:
                if (msg.equals("q")) {
                    printMainMenu();
                    break;
                }
                if (lpSensor.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED) {
                    errorMessage("Sensor not connected");
                    break;
                }
                if (msg.equals("0")) {
                    range = LpmsB2.LPMS_STREAM_FREQ_5HZ;
                } else if (msg.equals("1")) {
                    range = LpmsB2.LPMS_STREAM_FREQ_10HZ;
                } else if (msg.equals("2")) {
                    range = LpmsB2.LPMS_STREAM_FREQ_25HZ;
                } else if (msg.equals("3")) {
                    range = LpmsB2.LPMS_STREAM_FREQ_50HZ;
                } else if (msg.equals("4")) {
                    range = LpmsB2.LPMS_STREAM_FREQ_100HZ;
                } else if (msg.equals("5")) {
                    range = LpmsB2.LPMS_STREAM_FREQ_200HZ;
                } else if (msg.equals("6")) {
                    range = LpmsB2.LPMS_STREAM_FREQ_400HZ;
                } else {
                    errorMessage("Invalid selection, try again");
                    break;
                }
                setStreamFrequency(range);
                printMainMenu();
                break;

            case SET_FILTER_MODE_MENU:
                if (msg.equals("q")) {
                    printMainMenu();
                    break;
                }
                if (lpSensor.getConnectionStatus() != LpmsB2.SENSOR_STATUS_CONNECTED) {
                    errorMessage("Sensor not connected");
                    break;
                }
                if (msg.equals("0")) {
                    range = LpmsB2.LPMS_FILTER_GYR;
                } else if (msg.equals("1")) {
                    range = LpmsB2.LPMS_FILTER_GYR_ACC_KALMAN;
                } else if (msg.equals("2")) {
                    range = LpmsB2.LPMS_FILTER_GYR_ACC_MAG_KALMAN;
                } else if (msg.equals("1")) {
                    range = LpmsB2.LPMS_FILTER_GYR_ACC_DCM;
                } else if (msg.equals("2")) {
                    range = LpmsB2.LPMS_FILTER_GYR_ACC_MAG_DCM;
                } else {
                    errorMessage("Invalid selection, try again");
                    break;
                }
                setFilterMode(range);
                printMainMenu();
                break;
                

            case PRINT_DATA:
                stopPrintData();
                printMainMenu();

            default:
                printMainMenu();

        }
    }


    // Sensor interface
    boolean connectSensor(String port) {
        return lpSensor.connect(port, 921600); 
    }

    boolean disconnectSensor() {
        return lpSensor.disconnect();
    }

    void setCommandMode() {
        lpSensor.setCommandMode();
    }   

    void setStreamingMode() {
        lpSensor.setStreamingMode();
    }

    void setImuId(int id) {
        lpSensor.setImuId(id);
    }

    void setAccRange(int i) {
        lpSensor.setAccRange(i);
    }

    void setGyroRange(int i) {
        lpSensor.setGyroRange(i);
    }

    void setMagRange(int i) {
        lpSensor.setMagRange(i);
    }

    void setStreamFrequency(int i) {
        lpSensor.setStreamFrequency(i);
    }

    void setFilterMode(int i) {
        lpSensor.setFilterMode(i);
    }

    void getBatteryLevel() {
        lpSensor.getBatteryVoltage();
        lpSensor.getBatteryPercentage();
    }

    void getChargingStatus() {
        lpSensor.getChargingStatus();
    }

    public static void main(String args[]) {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    //Turn off metal's use of bold fonts
                    UIManager.put("swing.boldMetal", Boolean.FALSE);
                    new LpmsExample().setVisible(true);
                }
            }
        );


    }
    
   
}
