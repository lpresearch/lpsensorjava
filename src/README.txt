LpSensorJava library 

LpSensorJava library uses bluetooth SPP to communicate with LpmsB2 sensors.

===========
Dependencies:
===========
jssc : https://code.google.com/archive/p/java-simple-serial-connector/

===========
Compile:
===========
- Download jssc.jar and put it in a directory of choice. (Here we put it in lib/ directory)

Unix:
----------
compile:
$ javac -cp ./lib/jssc.jar com/lpresearch/*.java
$ javac  LpmsExample.java


run:
$ java -cp .:./lib/jssc.jar LpmsExample

Windows:
-------------
compile:
$ javac -cp .\lib\jssc.jar com\lpresearch\*.java
$ javac  LpmsExample.java

run:
$ java -cp ".;.\lib\jssc.jar" LpmsExample
