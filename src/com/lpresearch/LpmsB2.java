package com.lpresearch;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Enumeration;
import java.util.TooManyListenersException;
import java.util.concurrent.LinkedBlockingDeque;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;

public class LpmsB2 {
    //////////////////////////////////////////////
    // Stream frequency enable bits
    //////////////////////////////////////////////
    public static final int LPMS_STREAM_FREQ_5HZ = 5;
    public static final int LPMS_STREAM_FREQ_10HZ = 10;
    public static final int LPMS_STREAM_FREQ_25HZ = 25;
    public static final int LPMS_STREAM_FREQ_50HZ = 50;
    public static final int LPMS_STREAM_FREQ_100HZ = 100;
    public static final int LPMS_STREAM_FREQ_200HZ = 200;
    public static final int LPMS_STREAM_FREQ_400HZ = 400;
    public static final int LPMS_FILTER_GYR = 0;
    public static final int LPMS_FILTER_GYR_ACC_KALMAN = 1;
    public static final int LPMS_FILTER_GYR_ACC_MAG_KALMAN = 2;
    public static final int LPMS_FILTER_GYR_ACC_DCM = 3;
    public static final int LPMS_FILTER_GYR_ACC_MAG_DCM = 4;
    // Gyro Range
    public static final int LPMS_GYR_RANGE_125DPS  = 125;
    public static final int LPMS_GYR_RANGE_245DPS  = 245;
    public static final int LPMS_GYR_RANGE_250DPS  = 250;
    public static final int LPMS_GYR_RANGE_500DPS  = 500;
    public static final int LPMS_GYR_RANGE_1000DPS  = 1000;
    public static final int LPMS_GYR_RANGE_2000DPS  = 2000;
    // Acc Range
    public static final int LPMS_ACC_RANGE_2G = 2;
    public static final int LPMS_ACC_RANGE_4G = 4;
    public static final int LPMS_ACC_RANGE_8G = 8;
    public static final int LPMS_ACC_RANGE_16G = 16;
    // Mag Range
    public static final int LPMS_MAG_RANGE_4GAUSS = 4;
    public static final int LPMS_MAG_RANGE_8GAUSS = 8;
    public static final int LPMS_MAG_RANGE_12GAUSS =12;
    public static final int LPMS_MAG_RANGE_16GAUSS =16;
    public static final int PARAMETER_SET_DELAY =10;
    // Connection status
    public static final int SENSOR_STATUS_CONNECTED = 1;
    public static final int SENSOR_STATUS_CONNECTING = 2;
    public static final int SENSOR_STATUS_DISCONNECTED = 3;
    // Offset mode
    public static final int  LPMS_OFFSET_MODE_OBJECT = 0;
    public static final int  LPMS_OFFSET_MODE_HEADING = 1;
    public static final int  LPMS_OFFSET_MODE_ALIGNMENT = 2;

    final String TAG = "LpmsB2";
    final int PACKET_ADDRESS0   = 0;
    final int PACKET_ADDRESS1   = 1;
    final int PACKET_FUNCTION0  = 2;
    final int PACKET_FUNCTION1  = 3;
    final int PACKET_LENGTH0    = 4;
    final int PACKET_LENGTH1    = 5;
    final int PACKET_RAW_DATA   = 6;
    final int PACKET_LRC_CHECK0 = 7;
    final int PACKET_LRC_CHECK1 = 8;
    final int PACKET_END        = 9;

    //////////////////////////////////////////////
    // Command Registers
    //////////////////////////////////////////////
    final int REPLY_ACK             = 0;
    final int REPLY_NACK            = 1;
    final int UPDATE_FIRMWARE       = 2;
    final int UPDATE_IAP            = 3;
    final int GET_CONFIG            = 4;
    final int GET_STATUS            = 5;
    final int GOTO_COMMAND_MODE     = 6;
    final int GOTO_STREAM_MODE      = 7;
    final int GET_SENSOR_DATA       = 9;
    final int SET_TRANSMIT_DATA     = 10;
    final int SET_STREAM_FREQ       = 11;
    // Register value save and reset
    final int WRITE_REGISTERS      = 15;
    final int RESTORE_FACTORY_VALUE = 16;
    // Reference setting and offset reset
    final int RESET_REFERENCE      = 17;
    final int SET_ORIENTATION_OFFSET = 18;
    final int RESET_ORIENTATION_OFFSET= 82;
    //IMU ID setting
    final int SET_IMU_ID            = 20;
    final int GET_IMU_ID            = 21;
    // Gyroscope settings
    final int START_GYR_CALIBRA     = 22;
    final int ENABLE_GYR_AUTOCAL    = 23;
    final int ENABLE_GYR_THRES      = 24;
    final int SET_GYR_RANGE         = 25;
    final int GET_GYR_RANGE         = 26;
    // Accelerometer settings
    final int SET_ACC_BIAS          = 27;
    final int GET_ACC_BIAS          = 28;
    final int SET_ACC_ALIGN_MATRIX  = 29;
    final int GET_ACC_ALIGN_MATRIX  = 30;
    final int SET_ACC_RANGE         = 31;
    final int GET_ACC_RANGE         = 32;
    final int SET_GYR_ALIGN_BIAS    = 48;
    final int GET_GYR_ALIGN_BIAS    = 49;
    final int SET_GYR_ALIGN_MATRIX  = 50;
    final int GET_GYR_ALIGN_MATRIX  = 51;
    // Magnetometer settings
    final int SET_MAG_RANGE         = 33;
    final int GET_MAG_RANGE         = 34;
    final int SET_HARD_IRON_OFFSET  = 35;
    final int GET_HARD_IRON_OFFSET  = 36;
    final int SET_SOFT_IRON_MATRIX  = 37;
    final int GET_SOFT_IRON_MATRIX  = 38;
    final int SET_FIELD_ESTIMATE    = 39;
    final int GET_FIELD_ESTIMATE    = 40;
    final int SET_MAG_ALIGNMENT_MATRIX  = 76;
    final int SET_MAG_ALIGNMENT_BIAS = 77;
    final int SET_MAG_REFRENCE      = 78;
    final int GET_MAG_ALIGNMENT_MATRIX = 79;
    final int GET_MAG_ALIGNMENT_BIAS = 80;
    final int GET_MAG_REFERENCE     = 81;
    // Filter settings
    final int SET_FILTER_MODE       = 41;
    final int GET_FILTER_MODE       = 42;
    final int SET_FILTER_PRESET     = 43;
    final int GET_FILTER_PRESET     = 44;
    final int SET_LIN_ACC_COMP_MODE = 67;
    final int GET_LIN_ACC_COMP_MODE = 68;

    //////////////////////////////////////////////
    // Status register contents
    //////////////////////////////////////////////
    final int SET_CENTRI_COMP_MODE  = 69;
    final int GET_CENTRI_COMP_MODE  = 70;
    final int SET_RAW_DATA_LP       = 60;
    final int GET_RAW_DATA_LP       = 61;
    final int SET_TIME_STAMP        = 66;
    final int SET_LPBUS_DATA_MODE   = 75;
    final int GET_FIRMWARE_VERSION  = 47;
    final int GET_BATTERY_LEVEL     = 87;
    final int GET_BATTERY_VOLTAGE   = 88;
    final int GET_CHARGING_STATUS   = 89;
    final int GET_SERIAL_NUMBER     = 90;
    final int GET_DEVICE_NAME       = 91;
    final int GET_FIRMWARE_INFO     = 92;
    final int START_SYNC            = 96;
    final int STOP_SYNC             = 97;
    final int GET_PING              = 98;
    final int GET_TEMPERATURE       = 99;

    //////////////////////////////////////////////
    // Configuration register contents
    //////////////////////////////////////////////
    public static final int LPMS_GYR_AUTOCAL_ENABLED = 0x00000001 << 30;
    public static final int LPMS_LPBUS_DATA_MODE_16BIT_ENABLED = 0x00000001 << 22;
    public static final int LPMS_LINACC_OUTPUT_ENABLED = 0x00000001 << 21;
    public static final int LPMS_DYNAMIC_COVAR_ENABLED = 0x00000001 << 20;
    public static final int LPMS_ALTITUDE_OUTPUT_ENABLED = 0x00000001 << 19;
    public static final int LPMS_QUAT_OUTPUT_ENABLED = 0x00000001 << 18;
    public static final int LPMS_EULER_OUTPUT_ENABLED = 0x00000001 << 17;
    public static final int LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED = 0x00000001 << 16;
    public static final int LPMS_GYR_CALIBRA_ENABLED = 0x00000001 << 15;
    public static final int LPMS_HEAVEMOTION_OUTPUT_ENABLED = 0x00000001 << 14;
    public static final int LPMS_TEMPERATURE_OUTPUT_ENABLED = 0x00000001 << 13;
    public static final int LPMS_GYR_RAW_OUTPUT_ENABLED = 0x00000001 << 12;
    public static final int LPMS_ACC_RAW_OUTPUT_ENABLED = 0x00000001 << 11;
    public static final int LPMS_MAG_RAW_OUTPUT_ENABLED = 0x00000001 << 10;
    public static final int LPMS_PRESSURE_OUTPUT_ENABLED = 0x00000001 << 9;
    final int LPMS_STREAM_FREQ_5HZ_ENABLED      = 0x00000000;
    final int LPMS_STREAM_FREQ_10HZ_ENABLED     = 0x00000001;
    final int LPMS_STREAM_FREQ_25HZ_ENABLED     = 0x00000002;
    final int LPMS_STREAM_FREQ_50HZ_ENABLED     = 0x00000003;
    final int LPMS_STREAM_FREQ_100HZ_ENABLED    = 0x00000004;
    final int LPMS_STREAM_FREQ_200HZ_ENABLED    = 0x00000005;
    final int LPMS_STREAM_FREQ_400HZ_ENABLED    = 0x00000006;
    final int LPMS_STREAM_FREQ_MASK             = 0x00000007;

    final int MAX_BUFFER = 4096;
    final int DATA_QUEUE_SIZE = 64;
    // status
    int connectionStatus = SENSOR_STATUS_DISCONNECTED;

    // Protocol parsing related
    int rxState = PACKET_END;
    byte[] rxBuffer = new byte[MAX_BUFFER];
    byte[] txBuffer = new byte[MAX_BUFFER];
    byte[] rawTxData = new byte[MAX_BUFFER];
    byte[] rawRxBuffer = new byte[MAX_BUFFER];

    int currentAddress = 0;
    int currentFunction = 0;
    int currentLength = 0;
    int rxIndex = 0;
    byte b = 0;
    int lrcCheck = 0;
    int nBytes = 0;
    boolean waitForAck = false;
    boolean waitForData = false;
    byte inBytes[] = new byte[2];
    
    // SerialComm 
    String mAddress;
    ClientReadThread serial;

    // Settings related
    int imuId = 0;
    int gyrRange = 0;
    int accRange = 0;
    int magRange = 0;
    int streamingFrequency = 0;
    int filterMode = 0;
    boolean isStreamMode = true;

    int configurationRegister = 0;
    boolean configurationRegisterReady = false;
    String serialNumber = "";
    boolean serialNumberReady = false;
    String deviceName = "";
    boolean deviceNameReady = false;
    String firmwareInfo="";
    boolean firmwareInfoReady = false;
    String firmwareVersion;

    boolean accEnable = false;
    boolean gyrEnable = false;
    boolean magEnable = false;
    boolean angularVelEnable = false;
    boolean quaternionEnable = false;
    boolean eulerAngleEnable = false;
    boolean linAccEnable = false;
    boolean pressureEnable = false;
    boolean altitudeEnable = false;
    boolean temperatureEnable = false;
    boolean heaveEnable = false;
    boolean sixteenBitDataEnable = false;
    boolean resetTimestampFlag = false;

    boolean newDataFlag = false;
    LinkedBlockingDeque<LpmsData> dataQueue = new LinkedBlockingDeque<LpmsData>();
    LpmsData mLpmsData = new LpmsData();
    int frameCounter = 0;

    public LpmsB2() {
        connectionStatus = SENSOR_STATUS_DISCONNECTED;
    }
    
    /////////////////////////////////////////////////////////////////////
    // Public methods
    /////////////////////////////////////////////////////////////////////
    /**
     * @param  COM Port
     * @param  Baud rate
     * @return true or false
     */
    public boolean connect(String address,int baudrate){
        
        mAddress = address;

        // Check for active connection
        if (connectionStatus != SENSOR_STATUS_DISCONNECTED) {
            System.out.println(TAG+" Another connection establishing ");
            return false;
        }
        serial = new ClientReadThread(mAddress);
        if (!serial.connect(baudrate)) {
            System.out.println(TAG+" Error establishing connection to " + mAddress);
            return false;
        }
        try {
          Thread.sleep(1000); // wait for 1 second
      } catch (Exception e) {
          e.printStackTrace();
      }
        connectionStatus = SENSOR_STATUS_CONNECTING;
        _gotoCommandMode();
        _getSerialNumber();
        _getDeviceName();
        _getFirmwareInfo();
        _getSensorSettings();
        _gotoStreamingMode();

        frameCounter = 0;
        connectionStatus = SENSOR_STATUS_CONNECTED;
        return true;
    }

    /**
     * Disconnect sensor
     *
     * @return true or false
     */
    public boolean disconnect() {
        boolean res = false;
        if (connectionStatus == SENSOR_STATUS_DISCONNECTED)
            return true;
        
        if (serial == null)
            return true;
        res = serial.disconnect();
        connectionStatus = SENSOR_STATUS_DISCONNECTED;
        return res;
    }

    /**
     * Get current connected COMPORT 
     *
     * @return comport
     */
    public String getAddress() {
        return mAddress;
    }

    /**
     * Put sensor into command mode
     */
    public void setCommandMode()
    {
        if (!assertConnected())
            return;
        waitForAck = true;
        lpbusSetNone(GOTO_COMMAND_MODE);
        _waitForAckLoop();
        isStreamMode = false;
    }

    /**
     * Put sensor into streaming mode
     */
    public void setStreamingMode()
    {
        if (!assertConnected())
            return;
        waitForAck = true;
        lpbusSetNone(GOTO_STREAM_MODE);
        _waitForAckLoop();
        isStreamMode = true;
    }

    /**
     * Set sensor imu id
     *
     * @param id number
     */
    public void setImuId(final int id)
    {
        if (!assertConnected())
            return;

        new Thread(new Thread() {
            public void run() {
                boolean b = isStreamMode;
                setCommandMode();
                waitForAck = true;
                lpbusSetInt32(SET_IMU_ID, id);
                _waitForAckLoop();
                _getSensorSettings();
                _saveParameters();
                if (b)
                    setStreamingMode();
            }
        }).start();
    }
    
    /**
     * Get sensor imu id
     *
     * @return id number
     */
    public int getImuId()
    {
        return imuId;
    }

    /**
     * Get sensor gyro range
     *
     * @return gyro range (125, 245, 500, 1000, 2000 dps)
     */
    public int getGyroRange()
    {
        return gyrRange;
    }

    /**
     * Set sensor gyro range
     *
     * @param 
     * LPMS_GYR_RANGE_125DPS  125
     * LPMS_GYR_RANGE_245DPS  245
     * LPMS_GYR_RANGE_500DPS  500
     * LPMS_GYR_RANGE_1000DPS  1000
     * LPMS_GYR_RANGE_2000DPS  2000
     */
    public void setGyroRange(final int range)
    {
        if (!assertConnected())
            return;
        if (range == LPMS_GYR_RANGE_125DPS ||
                range == LPMS_GYR_RANGE_245DPS ||
                range == LPMS_GYR_RANGE_500DPS ||
                range == LPMS_GYR_RANGE_1000DPS ||
                range == LPMS_GYR_RANGE_2000DPS) {

            new Thread(new Thread() {
                public void run() {
                    boolean b = isStreamMode;
                    setCommandMode();
                    waitForAck=true;
                    lpbusSetInt32(SET_GYR_RANGE, range);
                    _waitForAckLoop();
                    _getSensorSettings();
                    _saveParameters();
                    if(b)
                        setStreamingMode();
                }
            }).start();
        }
    }

    /**
     * Get sensor acc range
     *
     * @return acc range (2, 4, 8, 16G)
     */
    public int getAccRange()
    {
        return accRange;
    }

    /**
     * Set sensor acc range
     *
     * @param 
     * LPMS_ACC_RANGE_2G  2
     * LPMS_ACC_RANGE_4G  4
     * LPMS_ACC_RANGE_8G  8
     * LPMS_ACC_RANGE_16G  16
     */
    public void setAccRange(final int range)
    {
        if (!assertConnected())
            return;
        if (range == LPMS_ACC_RANGE_2G ||
                range == LPMS_ACC_RANGE_4G ||
                range == LPMS_ACC_RANGE_8G ||
                range == LPMS_ACC_RANGE_16G ) {

            new Thread(new Thread() {
                public void run() {
                    boolean b = isStreamMode;
                    setCommandMode();
                    waitForAck = true;
                    lpbusSetInt32(SET_ACC_RANGE, range);
                    _waitForAckLoop();
                    _getSensorSettings();
                    _saveParameters();
                    if (b)
                        setStreamingMode();
                }
            }).start();
        }
    }


    /**
     * Get sensor mag range
     *
     * @return mag range (4, 8, 12, 16Gauss)
     */
    public int getMagRange()
    {
        return magRange;
    }

    /**
     * Set sensor mag range
     * @param 
     * LPMS_MAG_RANGE_4GAUSS  4
     * LPMS_MAG_RANGE_8GAUSS  8
     * LPMS_MAG_RANGE_12GAUSS 12
     * LPMS_MAG_RANGE_16GAUSS 16
     */
    public void setMagRange(final int range)
    {
        if (!assertConnected())
            return;
        if (range == LPMS_MAG_RANGE_4GAUSS ||
                range == LPMS_MAG_RANGE_8GAUSS ||
                range == LPMS_MAG_RANGE_12GAUSS ||
                range == LPMS_MAG_RANGE_16GAUSS ) {

            new Thread(new Thread() {
                public void run() {
                    boolean b = isStreamMode;
                    setCommandMode();
                    waitForAck = true;
                    lpbusSetInt32(SET_MAG_RANGE, range);
                    _waitForAckLoop();
                    _getSensorSettings();
                    _saveParameters();
                    if (b)
                        setStreamingMode();
                }
            }).start();
        }
    }

    /**
     * Get sensor filter mode
     *
     * @return filter mode
     * LPMS_FILTER_GYR 0
     * LPMS_FILTER_GYR_ACC_KALMAN 1
     * LPMS_FILTER_GYR_ACC_MAG_KALMAN 2
     * LPMS_FILTER_GYR_ACC_DCM 3
     * LPMS_FILTER_GYR_ACC_MAG_DCM 4
     */
    public int getFilterMode()
    {
        return filterMode;
    }

    /**
     * Set sensor filter mode
     *
     * @param filter mode
     * LPMS_FILTER_GYR 0
     * LPMS_FILTER_GYR_ACC_KALMAN 1
     * LPMS_FILTER_GYR_ACC_MAG_KALMAN 2
     * LPMS_FILTER_GYR_ACC_DCM 3
     * LPMS_FILTER_GYR_ACC_MAG_DCM 4
     */
    public void setFilterMode(final int mode)
    {
        if (!assertConnected())
            return;
        if (mode == LPMS_FILTER_GYR ||
                mode == LPMS_FILTER_GYR_ACC_KALMAN ||
                mode == LPMS_FILTER_GYR_ACC_MAG_KALMAN ||
                mode == LPMS_FILTER_GYR_ACC_DCM ||
                mode == LPMS_FILTER_GYR_ACC_MAG_DCM) {

            new Thread(new Thread() {
                public void run() {
                    boolean b = isStreamMode;
                    setCommandMode();
                    waitForAck = true;
                    lpbusSetInt32(SET_FILTER_MODE, mode);
                    _waitForAckLoop();
                    _getSensorSettings();
                    _saveParameters();
                    if (b)
                        setStreamingMode();
                }
            }).start();
        }
    }

    /**
     * Get sensor streaming frequency
     *
     * @return streaming frequency
     * LPMS_STREAM_FREQ_5HZ 5Hz
     * LPMS_STREAM_FREQ_10HZ 10Hz
     * LPMS_STREAM_FREQ_25HZ 25Hz
     * LPMS_STREAM_FREQ_50HZ 50Hz
     * LPMS_STREAM_FREQ_100HZ 100Hz
     * LPMS_STREAM_FREQ_200HZ 200Hz
     * LPMS_STREAM_FREQ_400HZ 400Hz
     */
    public int getStreamFrequency()
    {
        return streamingFrequency;
    }

    /**
     * Set sensor streaming frequency
     *
     * @param streaming frequency
     * LPMS_STREAM_FREQ_5HZ 5Hz
     * LPMS_STREAM_FREQ_10HZ 10Hz
     * LPMS_STREAM_FREQ_25HZ 25Hz
     * LPMS_STREAM_FREQ_50HZ 50Hz
     * LPMS_STREAM_FREQ_100HZ 100Hz
     * LPMS_STREAM_FREQ_200HZ 200Hz
     * LPMS_STREAM_FREQ_400HZ 400Hz
     */
    public void setStreamFrequency(final int freq)
    {
        if (!assertConnected())
            return;
        if (freq == LPMS_STREAM_FREQ_5HZ||
                freq == LPMS_STREAM_FREQ_10HZ ||
                freq == LPMS_STREAM_FREQ_25HZ ||
                freq == LPMS_STREAM_FREQ_50HZ ||
                freq == LPMS_STREAM_FREQ_100HZ ||
                freq == LPMS_STREAM_FREQ_200HZ ||
                freq == LPMS_STREAM_FREQ_400HZ ) {
            
            new Thread(new Thread() {
                public void run() {
                    boolean b = isStreamMode;
                    setCommandMode();
                    waitForAck = true;
                    lpbusSetInt32(SET_STREAM_FREQ, freq);
                    _waitForAckLoop();
                    _getSensorSettings();
                    _saveParameters();
                    if (b)
                        setStreamingMode();
                }
            }).start();
        }
    }

    /**
     * Set data to transmit
     *
     * @param v 32-bit integer
     * Example usage:
     * int transmissionData = 0;
     * transmissionData |= LpmsB2.LPMS_ACC_RAW_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_GYR_RAW_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_MAG_RAW_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_QUAT_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_EULER_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_LINACC_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_PRESSURE_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_ALTITUDE_OUTPUT_ENABLED;
     * transmissionData |= LpmsB2.LPMS_TEMPERATURE_OUTPUT_ENABLED;
     * setTransmissionData(transmissionData);
     */
    public void setTransmissionData(final int v)
    {
        if (!assertConnected())
            return;

        new Thread(new Thread() {
            public void run(){
                boolean b = isStreamMode;
                setCommandMode();
                waitForAck = true;
                lpbusSetInt32(SET_TRANSMIT_DATA, v);
                _waitForAckLoop();
                _getSensorSettings();
                _saveParameters();
                if (b)
                    setStreamingMode();
            }
        }).start();
    }

    /**
     * Gyroscope data output enabled
     * 
     * @param b true for enable .false for disable.
     */
    public void enableGyroData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_GYR_RAW_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_GYR_RAW_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isGyroDataEnabled() {
        return gyrEnable;
    }

    /**
     * Accelerometer data output enabled
     * 
     * @param b true for enable .false for disable.
     */
    public void enableAccData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_ACC_RAW_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_ACC_RAW_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isAccDataEnabled() {
        return accEnable;
    }

    /**
     * Magnetometer data output enabled
     * 
     * @param b true for enable .false for disable.
     */
    public void enableMagData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_MAG_RAW_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_MAG_RAW_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isMagDataEnabled() {
        return magEnable;
    }

    /**
     * Angular velocity output enabled
     *
     * @param b true for enable .false for disable.
     */
    public void enableAngularVelData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isAngularVelDataEnable() {
        return angularVelEnable;
    }

    /**
     * Quaternion orientation output enabled
     * 
     * @param b true for enable .false for disable.
     */
    public void enableQuaternionData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_QUAT_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_QUAT_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isQuaternionDataEnabled() {
        return quaternionEnable;
    }

    /**
     * Euler angle data output enabled
     * 
     * @param b true for enable .false for disable.
     */
    public void enableEulerData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_EULER_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_EULER_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isEulerDataEnabled() {
        return eulerAngleEnable;
    }

    /**
     * Linear acceleration data output enabled
     * 
     * @param b true for enable .false for disable.
     */
    public void enableLinAccData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_LINACC_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_LINACC_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isLinAccDataEnabled() {
        return linAccEnable;
    }

    /**
     * Pressure data output enabled
     * 
     * @param b true for enable .false for disable.
     */
    public void enablePressureData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_PRESSURE_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_PRESSURE_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isPressureDataEnabled(){
        return pressureEnable;
    }

    /**
     * Altitude data output enabled
     * 
     * @param b true for enable .false for disable.
     *  
     */
    public void enableAltitudeData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_ALTITUDE_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_ALTITUDE_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isAltitudeDataEnabled() {
        return altitudeEnable;
    }


    /**
     * Temperature data output enabled
     * 
     * @param b true for enable .false for disable.
     * 
     */
    public void enableTemperatureData(boolean b)
    {
        if (!assertConnected())
            return;
        if (b)
            configurationRegister |= LPMS_TEMPERATURE_OUTPUT_ENABLED;
        else
            configurationRegister &= ~LPMS_TEMPERATURE_OUTPUT_ENABLED;
        _setTransmissionData();
    }

    public boolean isTemperatureDataEnabled() {
        return temperatureEnable;
    }

    /**
     * Enable 16 bit data
     */
    public void enable16BitData()
    {
        if (!assertConnected())
            return;
        configurationRegister |= LPMS_LPBUS_DATA_MODE_16BIT_ENABLED;
        _setTransmissionData();
    }

    /**
     * Enable 32 bit data
     */
    public void enable32BitData()
    {
        if (!assertConnected())
            return;
        configurationRegister &= ~LPMS_LPBUS_DATA_MODE_16BIT_ENABLED;
        _setTransmissionData();
    }
  
    /**
     * Current data mode
     *
     * @return 
     * true: 16 bit data enabled
     * false: 32 bit data enabled 
     */
    public boolean is16BitDataEnabled()
    {
        if (sixteenBitDataEnable)
            return true;
        else
            return false;
    }

    /**
     * Check is new sensor data is available 
     *
     * @return 
     *  Return the number of sensor data in data queue
     */
    public int hasNewData() {
        int n;
        synchronized (dataQueue) {
            n = dataQueue.size();
        }
        return n;
    }

    /**
     * Retrieve data from data queue
     * 
     * @return LpmsData (first data in the FIFO queue)
     */
    public LpmsData getLpmsData() {
        LpmsData d=null;
        if (!assertConnected())
            return d;
        if (!isStreamMode) {

            synchronized (dataQueue) {
                while (dataQueue.size() > 0) {
                    d = dataQueue.getLast();
                    dataQueue.removeLast();
                }
            }
            waitForData = true;
            lpbusSetNone(GET_SENSOR_DATA);
            _waitForDataLoop();
        }else{
            synchronized (dataQueue) {
                if (dataQueue.size() > 0) {
                    d = dataQueue.getLast();
                    dataQueue.removeLast();
                }
            }
        }
        return d;
    }

    /**
     * Get sensor serial number
     *
     * @return sensor serial number
     */
    public String getSerialNumber()
    {
        return serialNumber;
    }

    /**
     * Get sensor name
     *
     * @return sensor name
     */
    public String getDeviceName()
    {
        return deviceName;
    }

    /**
     * Get firmware info
     *
     * @return firmware info
     */
    public String getFirmwareInfo()
    {
        return firmwareInfo;
    }

    /**
     * Is sensor is streaming mode
     *
     * @return 
     * true: sensor in streaming mode
     * false: sensor in command mode
     */
    public boolean isStreamingMode()
    {
        return isStreamMode;
    }

    /**
     * Get current connection status
     *
     * @return current connection status
     * SENSOR_STATUS_CONNECTED
     * SENSOR_STATUS_CONNECTING
     * SENSOR_STATUS_DISCONNECTED
     */
    public int getConnectionStatus() {
        return connectionStatus;
    }

    /**
     * Start sensor software sync for multiple connected sensors.
     * Puts sensor in sync mode. Must call stopSyncSensor() to put 
     * sensor back into normal operation mode.
     * Usage example:
     * startSyncSensor();
     * try {
     *     Thread.sleep(1000); // wait for 1 second
     * } catch (Exception e) {
     *     e.printStackTrace();
     * }
     * stopSyncSensor();
     *
     */
    public void startSyncSensor()
    {
        if (!assertConnected())
            return;
        lpbusSetNone(START_SYNC);
        waitForAck = true;
        _waitForAckLoop();
    }

    /**
     * Stop sensor software sync
     * Puts sensor in normal operation mode
     */
    public void stopSyncSensor()
    {
        if (!assertConnected())
            return;
        lpbusSetNone(STOP_SYNC);
        waitForAck = true;
        _waitForAckLoop();
    }

    /**
     * 
     * Sets the orientation offset using one of the three offset methods.
     * Orientation offset mode
     *
     * @param offset
     * LPMS_OFFSET_MODE_OBJECT for Object reset
     * LPMS_OFFSET_MODE_HEADING for Heading reset
     * LPMS_OFFSET_MODE_ALIGNMENT for Alignment reset
     */
    public void setOrientationOffset(int offset){

        if (!assertConnected())
            return;
        if (offset == LPMS_OFFSET_MODE_ALIGNMENT ||
                offset == LPMS_OFFSET_MODE_HEADING ||
                offset == LPMS_OFFSET_MODE_OBJECT) {
            lpbusSetInt32(SET_ORIENTATION_OFFSET, offset);
        }
    }

    /**
     * Reset the orientation offset to 0 (unity quaternion).
     * 
     */
    public void resetOrientationOffset() {
    
        if (!assertConnected())
            return;
        lpbusSetNone(RESET_ORIENTATION_OFFSET);
    }

    /**
     * Sets current sensor time stamp to 0.
     */
    public void resetTimestamp() {
        if (!assertConnected())
            return;
        lpbusSetInt32(SET_TIME_STAMP, 0);
    }
    
    /**
     * Sets current sensor time stamp. Sensor uses an internal integer
     * counter incrementing at 400Hz. Settings ts to 400 will current timestamp
     * to 1sec
     *
     * @param 
     * timestamp in seconds x 400 
     */
    public void setTimestamp(int ts) {

        if (!assertConnected())
            return;
        lpbusSetInt32(SET_TIME_STAMP, ts);
    }

    /**
     * Send command to retrive current battery in percentage.
     * Data will be available in subsequent LpmsData package
     *
     */
    public void getBatteryPercentage() {
        if (!assertConnected())
            return;
        lpbusSetNone(GET_BATTERY_LEVEL);
    }

    /**
     * Send command to retrive current battery in voltage.
     * Data will be available in subsequent LpmsData package
     *
     */
    public void getBatteryVoltage() {
        if (!assertConnected())
            return;
        lpbusSetNone(GET_BATTERY_VOLTAGE);
    }

    /**
     * Send command to retrive current charging status.
     * Data will be available in subsequent LpmsData package
     *
     */
    public void getChargingStatus() {
        if (!assertConnected())
            return;
        lpbusSetNone(GET_CHARGING_STATUS);
    }

    /**
     * Get current config register 
     *  
     * @return current config register
     *
     */
    public int getConfigRegister() {
        return configurationRegister;
    }

    /**
     * Get current config register in string format
     *  
     * @return current config register
     *
     */
    public String getConfigString() {
        String config = "";
        config += "Serial number: " + serialNumber + "\n";
        config += "Firmware version: " + firmwareInfo + "\n";
        config += "Device name: " + deviceName + "\n";
        config += "Imu Id: " + imuId + "\n";
        config += "Gyro range: " + gyrRange + "dps\n";
        config += "Acc range: " + accRange + "G\n";
        config += "Mag range: " + magRange + "Gauss\n";
        config += "Stream frequency: " + streamingFrequency + "Hz\n";

        config += "Filter mode: ";
        switch (filterMode) {
            case LPMS_FILTER_GYR:
                config += "Gyro only\n";
                break;
            case LPMS_FILTER_GYR_ACC_KALMAN:
                config += "Gyro + Acc (Kalman)\n";
                break;
            case LPMS_FILTER_GYR_ACC_MAG_KALMAN:
                config += "Gyro + Acc + Mag(Kalman)\n";
                break;
            case LPMS_FILTER_GYR_ACC_DCM:
                config += "Gyro + Acc (DCM)\n";
                break;
            case LPMS_FILTER_GYR_ACC_MAG_DCM:
                config += "Gyro + Acc + Mag(DCM)\n";
                break;
        }

        if (gyrEnable) {
            config += "O Gyro data\n";
        }else {
            config += "X Gyro data\n";
        }
        if ( accEnable) {
            config += "O Accelerometer data\n";
        }else {
            config += "X Accelerometer data\n";
        }
        if ( magEnable ) {
            config += "O Magnetomer data\n";
        } else {
            config += "X Magnetomer data\n";
        }
        if (angularVelEnable) {
            config += "O Angular Velocity data\n";
        } else {
            config += "X Angular Velocity data\n";
        }
        if (quaternionEnable) {
            config += "O Quaternion data\n";
        } else {
            config += "X Quaternion data\n";
        }
        if ( eulerAngleEnable) {
            config += "O Euler data\n";
        } else {
            config += "X Euler data\n";
        }
        if (linAccEnable) {
            config += "O Linear acceleration data\n";
        } else {
            config += "X Linear acceleration data\n";
        }
        if (pressureEnable) {
            config += "O Pressure data\n";
        } else {
            config += "X Pressure data\n";
        }
        if (altitudeEnable) {
            config += "O Altitude date\n";
        } else {
            config += "X Altitude date\n";
        }
        if (temperatureEnable) {
            config += "O Temperature data\n";
        } else {
            config += "X Temperature data\n";
        }

        if (sixteenBitDataEnable) {
           config += "O 16 bit data\n";
        } else {
            config += "O 32 bit data\n";
        }
        return config;
    }

    /**
     * Reset the LPMS parameters to factory default values. 
     * WARNING: CALIBRATION SETTINGS WILL BE ERASED, MAKE SURE
     * YOU HAVE SAVED SENSOR CALIBRATION DATA TO AN EXTERNAL FILE. 
     * 
     */
    public void resetFactorySettings()
    {
        if (!assertConnected())
            return;

        boolean b = isStreamMode;
        setCommandMode();
        waitForAck = true;
        lpbusSetNone(RESTORE_FACTORY_VALUE);
        _waitForAckLoop();
        _getSensorSettings();
        _saveParameters();
        if (b)
            setStreamingMode();
    }


    /////////////////////////////////////////////////////////////////////
    // LP Bus Related
    /////////////////////////////////////////////////////////////////////
    private void lpbusSetInt32(int command, int v) {
        for (int i = 0; i < 4; ++i) {
            rawTxData[i] = (byte) (v & 0xff);
            v = v >> 8;
        }
        sendData(command, 4);
    }

    private void lpbusSetNone(int command) {
        sendData(command, 0);
    }

    private void lpbusSetData(int command, int length, byte[] dataBuffer) {
        for (int i = 0; i < length; ++i) {
            rawTxData[i] = dataBuffer[i];
        }

        sendData(command, length);
    }

    private void parseSensorData() {
        int o = 0;
        float r2d = 57.2958f;

        mLpmsData.imuId = imuId;
        mLpmsData.timestamp = (float) convertRxbytesToInt(o, rxBuffer)*0.0025f;
        o += 4;
        if ( gyrEnable )
        {
            mLpmsData.gyr[0] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
            mLpmsData.gyr[1] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
            mLpmsData.gyr[2] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
        }

        if ( accEnable )
        {
            mLpmsData.acc[0] = (convertRxbytesToFloat(o, rxBuffer));
            o += 4;
            mLpmsData.acc[1] = (convertRxbytesToFloat(o, rxBuffer));
            o += 4;
            mLpmsData.acc[2] = (convertRxbytesToFloat(o, rxBuffer));
            o += 4;
        }

        if ( magEnable )
        {
            mLpmsData.mag[0] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
            mLpmsData.mag[1] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
            mLpmsData.mag[2] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
        }

        if ( angularVelEnable )
        {
            mLpmsData.angVel[0] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
            mLpmsData.angVel[1] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
            mLpmsData.angVel[2] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
        }

        if ( quaternionEnable )
        {
            mLpmsData.quat[0] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
            mLpmsData.quat[1] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
            mLpmsData.quat[2] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
            mLpmsData.quat[3] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
        }

        if ( eulerAngleEnable )
        {
            mLpmsData.euler[0] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
            mLpmsData.euler[1] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
            mLpmsData.euler[2] = convertRxbytesToFloat(o, rxBuffer) * r2d;
            o += 4;
        }

        if ( linAccEnable )
        {
            mLpmsData.linAcc[0] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
            mLpmsData.linAcc[1] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
            mLpmsData.linAcc[2] = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
        }

        if ( pressureEnable )
        {
            mLpmsData.pressure = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
        }

        if ( altitudeEnable )
        {
            mLpmsData.altitude = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
        }

        if ( temperatureEnable )
        {
            mLpmsData.temperature = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
        }

        if ( heaveEnable )
        {
            mLpmsData.heave = convertRxbytesToFloat(o, rxBuffer);
            o += 4;
        }

        synchronized (dataQueue) {
            if (dataQueue.size() < DATA_QUEUE_SIZE)
                dataQueue.addFirst(new LpmsData(mLpmsData));
            else {
                dataQueue.removeLast();
                dataQueue.addFirst(new LpmsData(mLpmsData));
            }
        }

        newDataFlag = true;
    }

    private void parseSensorData16Bit() {
        int o = 0;
        float r2d = 57.2958f;

        mLpmsData.imuId = imuId;
        mLpmsData.timestamp = (float) convertRxbytesToInt(0, rxBuffer)*0.0025f;

        o += 4;
        mLpmsData.frameNumber = frameCounter;
        frameCounter++;

        if ( gyrEnable )
        {
            for (int i = 0; i < 3; ++i) {
                mLpmsData.gyr[i] = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f * r2d;
                o += 2;
            }
        }

        if ( accEnable )
        {
            for (int i = 0; i < 3; ++i) {
                mLpmsData.acc[i] = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f;
                o += 2;
            }
        }

        if ( magEnable )
        {
            for (int i = 0; i < 3; ++i) {
                mLpmsData.mag[i] = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 100.0f;
                o += 2;
            }
        }

        if ( angularVelEnable )
        {
            for (int i = 0; i < 3; ++i) {
                mLpmsData.angVel[i] = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f* r2d;
                o += 2;
            }
        }

        if ( quaternionEnable )
        {
            for (int i = 0; i < 4; ++i) {
                mLpmsData.quat[i] = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 10000.0f;
                o += 2;
            }
        }

        if ( eulerAngleEnable )
        {
            for (int i = 0; i < 3; ++i) {
                mLpmsData.euler[i] = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 10000.0f * r2d;
                o += 2;
            }
        }

        if ( linAccEnable )
        {
            for (int i = 0; i < 3; ++i) {
                mLpmsData.linAcc[i] = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f;
                o += 2;
            }
        }

        if ( pressureEnable )
        {
            mLpmsData.pressure = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 100.0f;
            o += 2;
        }

        if ( altitudeEnable )
        {
            mLpmsData.altitude = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 10.0f;
            o += 2;
        }

        if ( temperatureEnable )
        {
            mLpmsData.temperature = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 100.0f;
            o += 2;
        }

        if ( heaveEnable )
        {
            mLpmsData.heave = (float) ((short) (((rxBuffer[o + 1]) << 8) | (rxBuffer[o + 0] & 0xff))) / 1000.0f;
            o += 2;
        }

        synchronized (dataQueue) {
            if (dataQueue.size() < DATA_QUEUE_SIZE)
                dataQueue.addFirst(new LpmsData(mLpmsData));
            else {
                dataQueue.removeLast();
                dataQueue.addFirst(new LpmsData(mLpmsData));
            }
        }

        newDataFlag = true;
    }

    private void parseFunction() {
        switch (currentFunction) {
            case REPLY_ACK:
                waitForAck = false;
                break;

            case REPLY_NACK:
                waitForAck = false;
                break;

            case GET_CONFIG:
                configurationRegister = convertRxbytesToInt(0, rxBuffer);
                configurationRegisterReady = true;
                //waitForData = false;
                break;

            case GET_STATUS:
                waitForData = false;
                break;

            case GET_SENSOR_DATA:
                if ( (configurationRegister & LPMS_LPBUS_DATA_MODE_16BIT_ENABLED) != 0) {
                    parseSensorData16Bit();
                } else {
                    parseSensorData();
                }
                waitForData = false;
                break;

            case GET_IMU_ID:
                imuId = convertRxbytesToInt(0, rxBuffer);
                waitForData = false;
                break;

            case GET_GYR_RANGE:
                gyrRange = convertRxbytesToInt(0, rxBuffer);
                waitForData = false;
                break;

            case GET_ACC_RANGE:
                accRange = convertRxbytesToInt(0, rxBuffer);
                waitForData = false;
                break;

            case GET_MAG_RANGE:
                magRange = convertRxbytesToInt(0, rxBuffer);
                waitForData = false;
                break;

            case GET_FILTER_MODE:
                filterMode = convertRxbytesToInt(0, rxBuffer);
                waitForData = false;
                break;

            case GET_BATTERY_LEVEL:
                mLpmsData.batteryLevel = convertRxbytesToFloat(0, rxBuffer);
                waitForData = false;
                break;

            case GET_CHARGING_STATUS:
                mLpmsData.chargingStatus = convertRxbytesToInt(0, rxBuffer);
                waitForData = false;
                break;

            case GET_BATTERY_VOLTAGE:
                mLpmsData.batteryVoltage = convertRxbytesToFloat(0, rxBuffer);
                waitForData = false;
                break;

            case GET_SERIAL_NUMBER:
                serialNumber = convertRxbytesToString(24, rxBuffer);
                serialNumberReady = true;
                waitForData = false;
                break;

            case GET_DEVICE_NAME:
                deviceName = convertRxbytesToString(16, rxBuffer);
                deviceNameReady = true;
                waitForData = false;
                break;

            case GET_FIRMWARE_INFO:
                firmwareInfo = convertRxbytesToString(16, rxBuffer);
                firmwareInfoReady = true;
                waitForData = false;
                break;

            case GET_FIRMWARE_VERSION:
                int vmajor = convertRxbytesToInt(8, rxBuffer);
                int vminor = convertRxbytesToInt(4, rxBuffer);
                int vbuild = convertRxbytesToInt(0, rxBuffer);
                firmwareVersion  = Integer.toString(vmajor)+"."+Integer.toString(vminor)+"."+ Integer.toString(vbuild);
                waitForData = false;
                break;

            case GET_PING:
                float mT;
                mT = (float) convertRxbytesToInt(0, rxBuffer)*0.0025f;
                waitForData = false;
                break;

            case START_SYNC:
                waitForAck = false;
                break;

            case STOP_SYNC:
                waitForAck = false;
                break;

            case SET_TRANSMIT_DATA:
                waitForData = false;
                break;

            case GET_TEMPERATURE:
                mLpmsData.temperature = convertRxbytesToFloat(0, rxBuffer);
                waitForData = false;
                break;
        }
    }

    private void parse() {
        int lrcReceived = 0;
        for (int i = 0; i < nBytes; i++) {
            b = rawRxBuffer[i];
            switch (rxState) {
                case PACKET_END:
                    if (b == 0x3a) {
                        rxState = PACKET_ADDRESS0;
                    }
                    break;

                case PACKET_ADDRESS0:
                    inBytes[0] = b;
                    rxState = PACKET_ADDRESS1;
                    break;

                case PACKET_ADDRESS1:
                    inBytes[1] = b;
                    currentAddress = convertRxbytesToInt16(0, inBytes);
                    imuId = currentAddress;
                    rxState = PACKET_FUNCTION0;
                    break;

                case PACKET_FUNCTION0:
                    inBytes[0] = b;
                    rxState = PACKET_FUNCTION1;
                    break;

                case PACKET_FUNCTION1:
                    inBytes[1] = b;
                    currentFunction = convertRxbytesToInt16(0, inBytes);
                    rxState = PACKET_LENGTH0;
                    break;

                case PACKET_LENGTH0:
                    inBytes[0] = b;
                    rxState = PACKET_LENGTH1;
                    break;

                case PACKET_LENGTH1:
                    inBytes[1] = b;
                    currentLength = convertRxbytesToInt16(0, inBytes);
                    rxState = PACKET_RAW_DATA;
                    rxIndex = 0;
                    break;

                case PACKET_RAW_DATA:
                    if (rxIndex == currentLength) {
                        lrcCheck = (currentAddress & 0xffff) + (currentFunction & 0xffff) + (currentLength & 0xffff);
                        for (int j = 0; j < currentLength; j++) {
                            if (j < MAX_BUFFER) {
                                lrcCheck += (int) rxBuffer[j] & 0xff;
                            } else {
                                rxState = PACKET_END;
                            }
                        }

                        inBytes[0] = b;
                        rxState = PACKET_LRC_CHECK1;
                    } else {
                        if (rxIndex < MAX_BUFFER) {

                            rxBuffer[rxIndex] = b;
                            rxIndex++;
                        } else {
                            rxState = PACKET_END;
                        }
                    }
                    break;

                case PACKET_LRC_CHECK1:
                    inBytes[1] = b;

                    lrcReceived = convertRxbytesToInt16(0, inBytes);
                    lrcCheck = lrcCheck & 0xffff;
                    
                    if (lrcReceived == lrcCheck) 
                    {
                        parseFunction();                   
                    }

                    rxState = PACKET_END;
                    break;

                default:
                    rxState = PACKET_END;
                    break;
            }
        }
    }

    private void sendData(int function, int length) {
        int txLrcCheck;

        txBuffer[0] = 0x3a;
        convertInt16ToTxbytes(imuId, 1, txBuffer);
        convertInt16ToTxbytes(function, 3, txBuffer);
        convertInt16ToTxbytes(length, 5, txBuffer);
        
        for (int i = 0; i < length; ++i) {
            txBuffer[7 + i] = rawTxData[i];
        }
        
        txLrcCheck = (imuId & 0xffff) + (function & 0xffff) + (length & 0xffff);

        for (int j = 0; j < length; j++) {
            txLrcCheck += (int) rawTxData[j] & 0xff;
        }

        convertInt16ToTxbytes(txLrcCheck, 7 + length, txBuffer);
        txBuffer[9 + length] = 0x0d;
        txBuffer[10 + length] = 0x0a;

        try {
            serial.writeBytes(txBuffer, length+11);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendAck() {
        sendData(REPLY_ACK, 0);
    }

    private void sendNack() {
        sendData(REPLY_NACK, 0);
    }

    private float convertRxbytesToFloat(int offset, byte buffer[]) {
        int l;                              
        
        l = buffer[offset + 0];                                  
        l &= 0xff;                                         
        l |= ((long) buffer[offset + 1] << 8);                   
        l &= 0xffff;                                       
        l |= ((long) buffer[offset + 2] << 16);                  
        l &= 0xffffff;                                     
        l |= ((long) buffer[offset + 3] << 24);
        
        return Float.intBitsToFloat(l);
    }

    private int convertRxbytesToInt(int offset, byte buffer[]) {
        int v;    
        v = (int) ((buffer[offset] & 0xFF)   
                | ((buffer[offset+1] & 0xFF)<<8)   
                | ((buffer[offset+2] & 0xFF)<<16)   
                | ((buffer[offset+3] & 0xFF)<<24));  
        return v;
        
    }

    private int convertRxbytesToInt16(int offset, byte buffer[]) {
        int v;
        //TODO
        v= (int) ((buffer[offset]&0xFF)   
                | ((buffer[offset+1]<<8) & 0xFF00)); 
        
        return v;
    }

    private String convertRxbytesToString(int length, byte buffer[]) {
        byte[] t = new byte[length];
        for (int i = 0; i < length; i++) {
            t[i] = buffer[i];
        }

        String decodedString = new String(t).trim();
        return decodedString;
    }

    private void convertIntToTxbytes(int v, int offset, byte buffer[]) {
        byte[] t = ByteBuffer.allocate(4).putInt(v).array();

        for (int i = 0; i < 4; i++) {
            buffer[3 - i + offset] = t[i];
        }
    }

    private void convertInt16ToTxbytes(int v, int offset, byte buffer[]) {
        byte[] t = ByteBuffer.allocate(2).putShort((short) v).array();

        for (int i = 0; i < 2; i++) {
            buffer[1 - i + offset] = t[i];
        }
    }

    private void convertFloatToTxbytes(float f, int offset, byte buffer[]) {
        int v = Float.floatToIntBits(f);
        byte[] t = ByteBuffer.allocate(4).putInt(v).array();

        for (int i = 0; i < 4; i++) {
            buffer[3 - i + offset] = t[i];
        }
    }

    private void _waitForAckLoop() {
        int timeout = 0;
        while (timeout++ < 100  && waitForAck)
        {
            try {
                Thread.sleep(PARAMETER_SET_DELAY);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void _waitForDataLoop() {
        int timeout = 0;
        while (timeout++ < 100 && waitForData)
        {
            try {
                Thread.sleep(PARAMETER_SET_DELAY);
            }
            catch (Exception e) {
                e.printStackTrace();
               // throw new RuntimeException(e);
            }
        }
    }

    private void _getSensorSettings() {
        _getConfig();
        _getGyroRange();
        _getAccRange();
        _getMagRange();
        _getFilterMode();
    }

    private void _getConfig() {
        configurationRegisterReady = false;
        lpbusSetNone(GET_CONFIG);
        int timeout = 0;
        while (timeout++ < 50  && !configurationRegisterReady)
        {
            try {
                Thread.sleep(PARAMETER_SET_DELAY);
            }
            catch (Exception e) {
                e.printStackTrace();
                // new RuntimeException(e);
            }
        }
        // Parse configuration
        parseConfig(configurationRegister);
    }


    private void parseConfig(int config ) {
        // Stream frequency
        if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_5HZ_ENABLED)
            streamingFrequency = LPMS_STREAM_FREQ_5HZ;
        else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_10HZ_ENABLED)
            streamingFrequency = LPMS_STREAM_FREQ_10HZ;
        else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_25HZ_ENABLED)
            streamingFrequency = LPMS_STREAM_FREQ_25HZ;
        else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_50HZ_ENABLED)
            streamingFrequency = LPMS_STREAM_FREQ_50HZ;
        else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_100HZ_ENABLED)
            streamingFrequency = LPMS_STREAM_FREQ_100HZ;
        else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_200HZ_ENABLED)
            streamingFrequency = LPMS_STREAM_FREQ_200HZ;
        else if ((configurationRegister & LPMS_STREAM_FREQ_MASK) == LPMS_STREAM_FREQ_400HZ_ENABLED)
            streamingFrequency = LPMS_STREAM_FREQ_400HZ;

        if ( (config & LPMS_GYR_RAW_OUTPUT_ENABLED) != 0) {
            gyrEnable = true;
        } else {
            gyrEnable = false;
        }
        if ( (config  &LPMS_ACC_RAW_OUTPUT_ENABLED) != 0) {
            accEnable = true;
        } else {
            accEnable = false;
        }
        if ( (config & LPMS_MAG_RAW_OUTPUT_ENABLED) != 0) {
            magEnable = true;
        } else {
            magEnable = false;
        }
        if ( (config & LPMS_ANGULAR_VELOCITY_OUTPUT_ENABLED) != 0) {
            angularVelEnable = true;
        } else {
            angularVelEnable = false;
        }
        if ( (config & LPMS_QUAT_OUTPUT_ENABLED) != 0) {
            quaternionEnable = true;
        } else {
            quaternionEnable = false;
        }
        if ( (config & LPMS_EULER_OUTPUT_ENABLED) != 0) {
            eulerAngleEnable = true;
        } else {
            eulerAngleEnable = false;
        }
        if ( (config & LPMS_LINACC_OUTPUT_ENABLED) != 0) {
            linAccEnable =true;
        } else {
            linAccEnable = false;
        }
        if ( (config & LPMS_PRESSURE_OUTPUT_ENABLED) != 0) {
            pressureEnable = true;
        } else {
            pressureEnable = false;
        }
        if ( (config & LPMS_TEMPERATURE_OUTPUT_ENABLED) != 0) {
            temperatureEnable = true;
        } else {
            temperatureEnable = false;
        }
        if ( (config & LPMS_ALTITUDE_OUTPUT_ENABLED) != 0) {
            altitudeEnable = true;
        } else {
            altitudeEnable = false;
        }
        if ( (config & LPMS_HEAVEMOTION_OUTPUT_ENABLED) != 0) {
            heaveEnable = true;
        } else {
            heaveEnable = false;
        } 
        if ( (config & LPMS_LPBUS_DATA_MODE_16BIT_ENABLED) != 0) {
            sixteenBitDataEnable = true;
        }  else {
            sixteenBitDataEnable = false;
        }
    }

    private void _gotoCommandMode()
    {
        waitForAck = true;
        lpbusSetNone(GOTO_COMMAND_MODE);
        _waitForAckLoop();
        isStreamMode = false;
    }

    /**
     * change sensor to Streaming mode.
     * it will send data too much ,and you have better to change sensor mode 
     * to set sensor or get some data you need.
     */
    private void _gotoStreamingMode()
    {
        waitForAck = true;
        lpbusSetNone(GOTO_STREAM_MODE);
        _waitForAckLoop();
        isStreamMode = true;
    }

    private void _getGyroRange()
    {
        waitForData = true;
        lpbusSetNone(GET_GYR_RANGE);
        _waitForDataLoop();
    }

    private void _getAccRange()
    {
        waitForData = true;
        lpbusSetNone(GET_ACC_RANGE);
        _waitForDataLoop();
    }

    private void _getMagRange()
    {
        waitForData = true;
        lpbusSetNone(GET_MAG_RANGE);
        _waitForDataLoop();
    }

    private void _getFilterMode()
    {
        waitForData = true;
        lpbusSetNone(GET_FILTER_MODE);
        _waitForDataLoop();
    }

    private void _getSerialNumber()
    {
        serialNumberReady = false;
        serialNumber = "";
        lpbusSetNone(GET_SERIAL_NUMBER);
        int timeout = 0;
        while (timeout++ < 30 && !serialNumberReady)
        {
            try {
                Thread.sleep(PARAMETER_SET_DELAY);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
    }
    private void _getDeviceName()
    {
        deviceNameReady = false;
        deviceName = "";
        lpbusSetNone(GET_DEVICE_NAME);
        int timeout = 0;
        while (timeout++ < 30 && !deviceNameReady)
        {
            try {
                Thread.sleep(PARAMETER_SET_DELAY);
            } catch (InterruptedException e) {

                e.printStackTrace();
            }
        }
    }

    private void _getFirmwareInfo()
    {
        firmwareInfoReady = false;
        firmwareInfo = "";
        lpbusSetNone(GET_FIRMWARE_INFO);
        int timeout = 0;
        while (timeout++ < 30 && !firmwareInfoReady)
        {
            try {
                Thread.sleep(PARAMETER_SET_DELAY);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                //throw new RuntimeException(e);
            }
        }
    }

    private void _saveParameters()
    {
        waitForAck = true;
        lpbusSetNone(WRITE_REGISTERS);
        _waitForAckLoop();
    }

    private void _setTransmissionData()
    {
        new Thread(new Thread() {
            public void run() {
                boolean b = isStreamMode;
                setCommandMode();
                waitForAck = true;
                lpbusSetInt32(SET_TRANSMIT_DATA, configurationRegister);
                _waitForAckLoop();
                _getSensorSettings();
                _saveParameters();
                if (b)
                    setStreamingMode();
            }
        }).start();
    }

    private boolean assertConnected()
    {
        if ( connectionStatus == SENSOR_STATUS_CONNECTED )
            return true;
        return false;
    }


    public class ClientReadThread implements SerialPortEventListener{
        String comPortName;
        SerialPort serialPort;
        boolean quit = true;
        boolean threadRunning = false;

        ClientReadThread (String port) {
            comPortName = port;
        }

        public boolean connect(int baudrate) {
            // Open com port  
            serialPort = new SerialPort(comPortName);
            try {
                serialPort.openPort(); 
                serialPort.setParams(baudrate, 1, 0, SerialPort.PARITY_NONE);
                serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_XONXOFF_IN | SerialPort.FLOWCONTROL_XONXOFF_OUT);    
                int mask = SerialPort.MASK_RXCHAR;
                //Set the prepared mask
                serialPort.setEventsMask(mask);
                //Add an interface through which we will receive information about events
                //serialPort.addEventListener(new SerialPortReader());
                serialPort.addEventListener(this);
            }
            catch (SerialPortException ex){
                System.out.println(ex);
                return false;
            }

            if (!serialPort.isOpened()) {
                connectionStatus = SENSOR_STATUS_DISCONNECTED;
                return false;
            }
            
            //start();
            return true;
        }

        public boolean disconnect() {
            boolean ret = false;
            if (serialPort == null)
                return true;
            try {
                ret = serialPort.removeEventListener();
                ret = serialPort.closePort();
            } catch (SerialPortException e) {
                System.out.println(e);
            }
            return ret;
        }

        public void writeBytes(byte[] buffer, int bytesToWrite) {
            byte[] buf = new byte[bytesToWrite];
            for (int i=0; i< bytesToWrite; ++i) {
                buf[i] = buffer[i];
            }
            try {
                serialPort.writeBytes(buf);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
 
        public void serialEvent(SerialPortEvent event) {
            if(event.isRXCHAR()){
                try {
                    rawRxBuffer = serialPort.readBytes();
                    if (rawRxBuffer != null) {
                        nBytes = rawRxBuffer.length;
                        parse();
                    }
                } catch (SerialPortException ex) {
                    System.out.println(ex);
                }
            }
        }
    }

    private void logd(String tag, String msg) {
        System.out.println("["+ TAG+"] " + msg);
    }
}